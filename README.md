# rpi4-docker

Example of https://docs.docker.com/build/building/multi-platform/ tutorial of building multi platform images.

## Start use QEMU emulation

    docker run --privileged --rm tonistiigi/binfmt --install all

    docker buildx create --use --name mybuild node-amd64
    docker buildx create --append --name mybuild node-arm64

    docker buildx create --name mybuilder --driver docker-container --bootstrap # --use
    docker buildx use mybuilder

# Build

    docker buildx build --provenance=false --platform linux/amd64,linux/arm64 --push -t registry.gitlab.com/vvlad1slavv/rpi4-docker:latest .
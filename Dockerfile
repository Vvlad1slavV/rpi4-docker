ARG BASE_IMG='ubuntu:20.04'

FROM ${BASE_IMG}
SHELL ["/bin/bash", "-ci"]

# Timezone Configuration
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y nano tmux wget curl lsb-release net-tools \
    xvfb glmark2 \
    p7zip-full && \
    rm -rf /var/lib/apt/lists/*

RUN cd $HOME && wget https://raw.githubusercontent.com/aikoncwd/rpi-benchmark/master/rpi-benchmark.sh && \
    chmod +x rpi-benchmark.sh
